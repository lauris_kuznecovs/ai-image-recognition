/**
 * Created by lauris.kuznecovs on 07/06/2017.
 */

export default class ImageRecognition {
  constructor(recognitionService, language = 'en', serviceModelId) {
    this.recognitionService = recognitionService;
    this.recognitionLanguage = language;
    this.serviceModelId = serviceModelId;
    this.url = '';
    this.tags = [];
    this.loadingText = 'Please wait, loading...';
  }
  addTag(title, threshold) {
    this.tags.push({
      title,
      threshold,
    });
  }
  getRecognitionData(imageUrl = '') {
    if (imageUrl === '') return;

    this.showLoadingScreen();
    this.url = imageUrl;
    this.tags = [];
    this.recognitionService.models
      .predict({ id: this.serviceModelId, language: this.recognitionLanguage }, imageUrl)
      .then((response) => {
        console.log(response.outputs[0].data.concepts);

        for (const concept of response.outputs[0].data.concepts) {
          this.addTag(concept.name, concept.value);
        }

        this.drawRecognitionData();
      })
      .catch((error) => {
        alert(`Image recognition error - ${error.statusText}`);
      });
  }
  drawRecognitionData() {
    document.getElementById('imageContainer').innerHTML = `
      <div class="flex-item">
          <img src="${this.url}">
      </div>
      <div class="flex-item">
          <h1>This is what I recognize</h1>
          <ul class="tags">
            ${
              this.tags.map(tag => (
                `<li>
                            <div>${tag.title}</div>
                            <div>${tag.threshold.toPrecision(3)}</div>
                        </li>`
              )).join('')
              }
          </ul>
      </div>
    `;
  }
  showLoadingScreen() {
    document.getElementById('imageContainer').innerHTML = `<h2>${this.loadingText}</h2>`;
  }
}
