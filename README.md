### What is this repository for? ###

* This is only for demonstration how AI (artificial intelligence) can work with PURE Javascript (ES6)
* If you have some advice or some improvements, feel free to push them in branches with following pattern:


```
#!bash

"{bugfix / feature / upgrade}-{describable keyword}"
```


### How to setup ###

*Required

NodeJS - [https://nodejs.org/en/download/package-manager/](Link URL)


*Run from terminal

```
#!bash
npm install
```


### Contribution guidelines ###

If you are editing code you must run following command:


```
#!bash
webpack --progress --watch
```

**!!! Never ever try to push into master**