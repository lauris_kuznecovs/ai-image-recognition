/**
 * Created by lauris.kuznecovs on 07/06/2017.
 */

import { clientId, clientSecret } from './src/config';
import ImageRecognition from './src/imageRecognitionClass';

// Initialize App
const recognitionService = new Clarifai.App(clientId, clientSecret);
const APIModelId = Clarifai.GENERAL_MODEL;
const APILanguage = 'en';
const imageRecognition = new ImageRecognition(recognitionService, APILanguage, APIModelId);

// Listen on Events
document.addEventListener('DOMContentLoaded', () => {
  document.getElementById('imageUrlSubmit').addEventListener('click', () => {
    // Push image URL to AI engine & load image with tags afterwards
    imageRecognition.getRecognitionData(document.getElementById('imageUrl').value);
  });
});
